package com.soaint.poc.consume.sp.repository.persona;

import com.soaint.poc.consume.sp.model.entities.Persona;
import com.soaint.poc.consume.sp.repository.persona.impl.PersonaRepositoryFacade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface IPersonaRepository extends JpaRepository<Persona, Long> {

    @Query("select p from Persona p where p.id = :id")
    Persona findPersonaByIdPersona(@Param("id") final Long id);
}
