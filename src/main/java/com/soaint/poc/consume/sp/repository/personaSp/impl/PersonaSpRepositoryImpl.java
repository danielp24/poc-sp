package com.soaint.poc.consume.sp.repository.personaSp.impl;

import com.soaint.poc.consume.sp.commons.converter.PersonaConverter;
import com.soaint.poc.consume.sp.commons.domains.generic.PersonaDTO;
import com.soaint.poc.consume.sp.model.entities.Persona;
import com.soaint.poc.consume.sp.repository.personaSp.PersonaSpRepositoryFacade;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Component
@Transactional
public class PersonaSpRepositoryImpl implements PersonaSpRepositoryFacade {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<PersonaDTO> getAllPersona() {
        try {
            StoredProcedureQuery query = this.em.createStoredProcedureQuery("get_all_personas");
            query.registerStoredProcedureParameter("r1", void.class, ParameterMode.REF_CURSOR);
            query.execute();
            List<Object[]> resul = query.getResultList();
            List<PersonaDTO> resultadoo = PersonaConverter.convertirElementosListToPersonaDTO(resul);
            System.out.println(resultadoo);

            return resultadoo;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
