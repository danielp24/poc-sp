package com.soaint.poc.consume.sp.commons.domains.request;

import com.soaint.poc.consume.sp.commons.domains.generic.PersonaDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class PersonaDTORequest extends PersonaDTO {


}
