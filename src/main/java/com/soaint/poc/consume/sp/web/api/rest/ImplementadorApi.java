package com.soaint.poc.consume.sp.web.api.rest;

import com.soaint.poc.consume.sp.commons.domains.generic.PersonaDTO;
import com.soaint.poc.consume.sp.commons.domains.request.PersonaDTORequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ImplementadorApi {

    ResponseEntity create(final PersonaDTORequest persona);

    ResponseEntity findPersonas();

    ResponseEntity findPersonById(final Long id);

    ResponseEntity updatePersonaById(final PersonaDTORequest persona, Long id);

    ResponseEntity deletePersonaById(final Long id);

    List<PersonaDTO> getAllPersonas();

}
