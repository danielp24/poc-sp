package com.soaint.poc.consume.sp.model.entities;

import com.soaint.poc.consume.sp.model.entities.base.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "PERSONA")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false, of = {"numeroIdentificacion"})
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = Persona.namedStoredProcedure,
                procedureName = "GET_ALL_PERSONAS",
                resultClasses = Persona.class)
})

public class Persona extends BaseEntity implements Serializable {

    public static final String namedStoredProcedure = "getAllPersonas";

    @Column(name = "TIPOIDENTIFICACION")
    private String tipoIdentificacion;
    @Column(name = "NUMEROIDENTIFICACION")
    private Long numeroIdentificacion;
    @Column(name = "NOMBRES")
    private String nombres;
    @Column(name = "APELLIDOS")
    private String apellidos;
    @Column(name = "CREATEDATE")
    @Temporal(TemporalType.DATE)
    private Date createDate;
}
