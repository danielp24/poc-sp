package com.soaint.poc.consume.sp.service.implementador.impl;

import com.soaint.poc.consume.sp.commons.converter.PersonaConverter;
import com.soaint.poc.consume.sp.commons.domains.generic.PersonaDTO;
import com.soaint.poc.consume.sp.commons.domains.request.PersonaDTORequest;
import com.soaint.poc.consume.sp.model.entities.Persona;
import com.soaint.poc.consume.sp.repository.persona.impl.PersonaRepositoryFacade;
import com.soaint.poc.consume.sp.repository.personaSp.PersonaSpRepositoryFacade;
import com.soaint.poc.consume.sp.service.implementador.IGestionPersona;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GestionPersonaImpl implements IGestionPersona {

    @Autowired
    private ModelMapper modelMapper;

    private final PersonaRepositoryFacade repository;
    private final PersonaSpRepositoryFacade facade;

    @Autowired
    public GestionPersonaImpl(PersonaRepositoryFacade repository, PersonaSpRepositoryFacade facade) {
        this.repository = repository;
        this.facade = facade;
    }


    @Override
    public Optional<PersonaDTO> registerPersona(PersonaDTORequest persona) {
        return Optional.ofNullable(PersonaConverter.entityToDtoPersona(repository.registerPersona(persona)));
    }

    @Override
    public Optional<Collection<PersonaDTO>> findPersonas() {

        return Optional.ofNullable(repository.findPersonas().get().stream()
                .map(persona -> modelMapper.map(persona, PersonaDTO.class))
                .collect(Collectors.toList()));
    }

    @Override
    public Optional<PersonaDTO> updatePersona(PersonaDTORequest persona, Long id) {
        return Optional.ofNullable(PersonaConverter.entityToDtoPersona(repository.updatePersona(persona, id)));
    }

    @Override
    public Optional<String> detelePersona(Long id) {
        Optional<String> ms = repository.deletePersona(id);
        return Optional.ofNullable(ms.get());
    }

    @Override
    public PersonaDTO getPersonaById(Long id) {
        Optional<Persona> persona = repository.getPersonaById(id);
        return persona.isPresent() ? modelMapper.map(persona.get(), PersonaDTO.class) : new PersonaDTO();
    }

    @Override
    public List<PersonaDTO> getAllPersonas() {
        return facade.getAllPersona();

    }
}
