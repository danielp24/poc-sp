package com.soaint.poc.consume.sp.repository.personaSp;

import com.soaint.poc.consume.sp.commons.domains.generic.PersonaDTO;

import java.util.List;

public interface PersonaSpRepositoryFacade {

    List<PersonaDTO> getAllPersona();
}
