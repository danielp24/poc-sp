package com.soaint.poc.consume.sp.commons.converter;

import com.soaint.poc.consume.sp.commons.domains.generic.PersonaDTO;
import com.soaint.poc.consume.sp.commons.util.DateUtil;
import com.soaint.poc.consume.sp.model.entities.Persona;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonaConverter {


    public static PersonaDTO entityToDtoPersona(Optional<Persona> persona) {
        PersonaDTO personaDTO = new PersonaDTO();
        if (persona.isPresent()) {
            personaDTO.setId(persona.get().getId());
            personaDTO.setNombres(persona.get().getNombres());
            personaDTO.setApellidos(persona.get().getApellidos());
            personaDTO.setNumeroIdentificacion(persona.get().getNumeroIdentificacion());
            personaDTO.setTipoIdentificacion(persona.get().getTipoIdentificacion());
            personaDTO.setCreateDate(persona.get().getCreateDate());
        }
        return personaDTO;
    }

    public static PersonaDTO entityToDtoPersona(Persona persona) {
        PersonaDTO personaDTO = new PersonaDTO();
        if (persona != null) {
            personaDTO.setId(persona.getId());
            personaDTO.setNombres(persona.getNombres());
            personaDTO.setApellidos(persona.getApellidos());
            personaDTO.setNumeroIdentificacion(persona.getNumeroIdentificacion());
            personaDTO.setTipoIdentificacion(persona.getTipoIdentificacion());
            personaDTO.setCreateDate(persona.getCreateDate());
        }
        return personaDTO;
    }

    public static List<Persona> convertirElementosList(List<Object[]> list) {
        List<Persona> resultado = new ArrayList<>();
        for (Object objeto[] : list) {
            Persona persona = new Persona();
            persona.setTipoIdentificacion(objeto[0].toString());
            persona.setNumeroIdentificacion(new Long(objeto[1].toString()));
            persona.setNombres(objeto[2].toString());
            persona.setApellidos(objeto[3].toString());
            persona.setCreateDate(DateUtil.getDateFromString(objeto[4].toString()));

            resultado.add(persona);
        }
        return resultado;
    }

    public static List<PersonaDTO> convertirElementosListToPersonaDTO(List<Object[]> list) {
        List<PersonaDTO> resultado = new ArrayList<>();
        for (Object objeto[] : list) {
            PersonaDTO persona = PersonaDTO.builder()
                    .apellidos(objeto[3].toString())
                    .nombres(objeto[2].toString())
                    .numeroIdentificacion(new Long(objeto[1].toString()))
                    .tipoIdentificacion(objeto[0].toString())
                    .createDate(DateUtil.getDateFromString(objeto[4].toString()))
                    .build();
            resultado.add(persona);
        }
        return resultado;
    }
}
