package com.soaint.poc.consume.sp.service.implementador;

import com.soaint.poc.consume.sp.commons.domains.generic.PersonaDTO;
import com.soaint.poc.consume.sp.commons.domains.request.PersonaDTORequest;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface IGestionPersona {

    Optional<PersonaDTO> registerPersona(final PersonaDTORequest persona);

    Optional<Collection<PersonaDTO>> findPersonas();

    PersonaDTO getPersonaById(final Long id);

    Optional<PersonaDTO> updatePersona(final PersonaDTORequest persona, final Long id);

    Optional<String> detelePersona(final Long id);

    List<PersonaDTO> getAllPersonas();

}
