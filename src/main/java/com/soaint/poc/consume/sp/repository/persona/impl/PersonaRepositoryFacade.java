package com.soaint.poc.consume.sp.repository.persona.impl;

import com.soaint.poc.consume.sp.commons.domains.request.PersonaDTORequest;
import com.soaint.poc.consume.sp.model.entities.Persona;

import java.util.Collection;
import java.util.Optional;

public interface PersonaRepositoryFacade {

    Optional<Persona> registerPersona(final PersonaDTORequest persona);

    Optional<Collection<Persona>>findPersonas();

    Optional<Persona> updatePersona(final PersonaDTORequest persona, Long id);

    Optional<Persona> getPersonaById(final Long id);

    Optional<String> deletePersona(final Long id);



}
